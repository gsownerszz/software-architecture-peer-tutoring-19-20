package wijnen;

/**
 * PEER opdracht
 * P2W2
 */
public class WijnHuis {
    private static final int MAX_AANTAL = 10;
    private Wijn[] wijnen = new Wijn[MAX_AANTAL];  //voorlopig gevuld met 10 null-objecten
    private String naam;
    private int aantal;

    public WijnHuis(String naam) {
        this.naam = naam;
    }

    public void voegWijnToe(Wijn wijn) {
        if (!zoekWijn(wijn) && aantal < MAX_AANTAL){
            wijnen[aantal] = wijn;
            aantal++;
        }
    }

    public boolean zoekWijn(Wijn wijn) {
        if (aantal == 0){
            return  false;
        }
        for (Wijn element : wijnen) {
            if (element != null && element.getNaam().equalsIgnoreCase(wijn.getNaam())){
                return true;
            }
        }
        return false;
    }

    public Wijn getOudsteWijn() {
        if (aantal == 0){
            return null;
        }
        Wijn oudste = wijnen[0];
        for (Wijn element : wijnen) {
            if (element.getOogstDatum().isBefore(oudste.getOogstDatum())){
                oudste = element;
            }
        }
        return oudste;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(String.format("Wijnhuis %s\n", naam.toUpperCase()));

        StringBuilder wijnenTekst = new StringBuilder();
        StringBuilder champagneTekst = new StringBuilder();
        StringBuilder likeurenTekst = new StringBuilder();
        wijnenTekst.append("Wijnen:\n");
        champagneTekst.append("Champagne:\n");
        likeurenTekst.append("Likeuren:\n");
        for (Wijn element : wijnen) {
            if (element != null){
                if (element instanceof Likeur){
                    likeurenTekst.append("\t" + element.toString() + "\n");
                }else if (element instanceof Champagne){
                    champagneTekst.append("\t" + element.toString() + "\n");
                }else {
                    wijnenTekst.append("\t" + element.toString() + "\n");
                }
            }
        }
        result.append(wijnenTekst).append(champagneTekst).append(likeurenTekst);
        return result.toString();
    }
}
