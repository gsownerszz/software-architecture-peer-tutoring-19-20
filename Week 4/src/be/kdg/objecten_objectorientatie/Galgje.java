package be.kdg.objecten_objectorientatie;

import java.util.ArrayList;
import java.util.Scanner;

public class Galgje {
    private static boolean end;
    private static int rounds = 0;
    private final static Scanner scanner = new Scanner(System.in);
    private static String word;
    private static StringBuilder found;
    private static ArrayList<String> galgje = new ArrayList<>();
    public static void main(String[] args) {
        createGalgje();
        word = requestWord();
        found = repeat(word.length(),".");
        while (!end){
            showRemainingCharacters();
            String input = requestInput();
            if (input.length()>1){
                makeGuess(input);
            }else {
                guessLetter(input.charAt(0));
            }
            if (rounds ==8){
                showRemainingCharacters();
                lose();
            }
        }
    }
    private static String requestWord(){
        System.out.print("Geef een woord (max 10 letters): ");
        String word = scanner.nextLine();
        if (word.length() < 5 || word.length() > 10){
            return requestWord();
        }
        return word;
    }

    private static void showRemainingCharacters(){
        System.out.println("Het te zoeken woord: " + found);
        for (int i = 0; i < rounds; i++) {
            System.out.println(galgje.get(i));
        }

    }
    private static void makeGuess(String guess){
        rounds++;
        if (word.equalsIgnoreCase(guess)){
            win();
        }
    }
    private static void guessLetter(char letter){
        rounds++;
        if (word.contains(String.valueOf(letter))){
            for (int i = 0; i < word.length() ; i++) {
                if (word.charAt(i) == letter){
                    found.setCharAt(i,letter);
                }
            }
        }
        if (word.equalsIgnoreCase(found.toString())){
            win();
        }
    }
    private static void win(){
        System.out.printf("Proficiat, je hebt het woord geraden in %d beurten!", rounds);
        end = true;
    }
    private static String requestInput(){
        System.out.print("Raad het woord of geef een letter: ");
        String input = scanner.nextLine();
        if (input.isBlank()){
            return requestInput();
        }
        return input;
    }
    private static StringBuilder repeat(int i, String s) {
        StringBuilder sb = new StringBuilder();
        for (int j = 0; j < i; j++)
            sb.append(s);
        return sb;
    }
    private static void createGalgje(){
        galgje.add("-----");
        galgje.add("|   |");
        galgje.add("|   O ");
        galgje.add("|  /|\\");
        galgje.add("|   |");
        galgje.add("|  / \\");
        galgje.add("|     ");
        galgje.add("-------");
    }
    private static void lose(){
        System.out.println("U hebt het woord niet kunnen gokken in 8 rondes. Het woord was: " + word);
        end = true;
    }
}
