package be.kdg.arrays.chat.robot;

import java.util.Date;

public class Utils {
    public static String[] getTemplateAnswersForBot1() {
        return new String[]{
                "Ok, probeer eerst al eens te herstarten",
                "Kan je het probleem eens herformuleren?",
                "En is dat al lang zo?",
                "Waarom heb je ons niet eerder gecontacteerd?",
                "Ik denk dat je dat zelf wel kan oplossen, niet?",
                "Die vraag heb ik nog niet vaak gehad!",
                "Sorry, ik was even bezig, kan je de vraag eens herhalen?",
                "Aha, gekend probleem, ik zoek het op en laat je iets weten.",
                "Sorry, daar heb ik niet direct een antwoord op. Wat nu?",
                "En dan?",
                "Dat zal wel, maar er zijn ergere dingen he.",
                "Hmm, die moet ik opzoeken. En wanneer deed dat zich juist voor?"
        };
    }
    public static String[] getTemplateAnswersForBot2() {
        return new String[]{
                "Probeer opnieuw aan te loggen, lost dat je probleem op?",
                "Herstarten van het systeem kan vele snelheidsproblemen oplossen. Heb je dat al geprobeerd?",
                "Misschien is er iets mis met de video adapter?",
                "Je kan best nog even wachten, mogelijk lost het probleem zichzelf op.",
                "Heb je onlangs nieuwe videosoftware geïnstalleerd?",
                "Is er nog voldoende ruimte vrij op het toestel?",
                "Het volume staat toch juist he?",
                "Mogelijk een probleem met één van de systeemdrivers.",
                "Pas op, soms wel hoor.",
                "En eergisteren?",
                "In de week werken die dingen meestal vlotter...",
                "Niet wanhopen, we vinden samen wel een oplossing.",
                "Ik zal het wel oplossen, geef jij mij gewoon wat meer info.",
                "Echt geen?",
                "Ik ken dat 'zeker', uiteindelijk blijkt het toch niet!"
        };
    }
    public static String[] getKeywords(){
        return new String[]{
                "aanloggen",
                "traag",
                "scherm",
                "hangt",
                "kleuren",
                "trager",
                "geluid",
                "vooruit",
                "niet",
                "gisteren",
                "weekend",
                "geen idee",
                "oplossing",
                "geen",
                "zeker"
        };
    }
    public static boolean detectProfanity(String input){
        for (String profanity: getProfanityWords()) {
            if (input.contains(profanity)){
                return true;
            }
        }
        return false;
    }
    private static String[] getProfanityWords(){
        return new String[]{"fuck","shit"};
    }
    public static long calculateSecondsDifferenceBetweenDates(Date first, Date later){
        return Math.round((double) (later.getTime()-first.getTime())/1000);
    }
}
