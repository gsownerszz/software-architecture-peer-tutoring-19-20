package be.kdg.arrays.chat.robot;

import java.util.Random;

public class ChatRobotVersion2 extends  ChatRobot {
    private Random random;
    public ChatRobotVersion2(String naam) {
        super(naam);
        random = new Random();
    }

    @Override
    public String antwoordOpVraag(String vraag) {
        if (vraag.length() < 4){
            return super.antwoordOpVraag(vraag);
        }
        for (String woord: vraag.split(" ")) {
            for (int i = 0; i < Utils.getKeywords().length; i++) {
                String zoekwoord = Utils.getKeywords()[i];
                if (woord.equalsIgnoreCase(Utils.getKeywords()[i])){

                    return Utils.getTemplateAnswersForBot2()[i];
                }
            }
        }
        return Utils.getTemplateAnswersForBot1()[random.nextInt(Utils.getTemplateAnswersForBot1().length)];
    }

    @Override
    public String stop() {
        return super.stop();
    }
}
