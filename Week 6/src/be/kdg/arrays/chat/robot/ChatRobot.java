package be.kdg.arrays.chat.robot;

import java.util.Calendar;
import java.util.Date;

public class ChatRobot implements IChatRobot {
    private String naam;
    private Date creationDate;
    public ChatRobot(String naam){
        this.naam = naam;
        this.creationDate = Calendar.getInstance().getTime();
    }

    @Override
    public String toString() {
        return String.format("Hallo, ik ben chatbot %s, stel me een vraag en ik geef je een oplossing!",naam);
    }

    public String stop(){
        return String.format("Je hebt blijkbaar geen vragen meer. Ok, dan ben ik weg. De groeten van %s",naam);
    }

    @Override
    public String antwoordOpVraag(String vraag) {
        return "";
    }
    public long getExtraSeconds(){
        if (Utils.calculateSecondsDifferenceBetweenDates(creationDate,Calendar.getInstance().getTime()) <= 60){
            return 0;
        }
        return Utils.calculateSecondsDifferenceBetweenDates(creationDate,Calendar.getInstance().getTime());
    }
}
