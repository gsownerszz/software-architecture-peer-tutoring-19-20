package be.kdg.arrays.chat.robot;

import java.util.Random;

public class ChatRobotVersion3 extends ChatRobot {
    private int questionsAsked;
    private String zucht;
    private Random random;
    public ChatRobotVersion3(String naam) {
        super(naam);
        random = new Random();
        zucht = "";
    }

    @Override
    public String antwoordOpVraag(String vraag) {
        if (vraag.length() < 4){
            return super.antwoordOpVraag(vraag);
        }
        questionsAsked++;
        if (questionsAsked > 5){
            zucht = "Zucht, typisch, ";
        }
        for (String woord: vraag.split(" ")) {
            for (int i = 0; i < Utils.getKeywords().length; i++) {
                if (woord.equalsIgnoreCase(Utils.getKeywords()[i])){
                    return zucht + Utils.getTemplateAnswersForBot2()[i];
                }
            }
        }
        return Utils.getTemplateAnswersForBot1()[random.nextInt(Utils.getTemplateAnswersForBot1().length)];
    }

    @Override
    public String stop() {
        return super.stop();
    }
}
