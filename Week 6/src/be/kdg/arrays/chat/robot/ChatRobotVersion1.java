package be.kdg.arrays.chat.robot;

import java.util.Random;

public class ChatRobotVersion1  extends  ChatRobot{
    private Random random;
    public ChatRobotVersion1(String naam) {
        super(naam);
        random = new Random();
    }

    @Override
    public String antwoordOpVraag(String vraag) {
        if (vraag.length() < 4){
            return super.antwoordOpVraag(vraag);
        }
        return Utils.getTemplateAnswersForBot1()[random.nextInt(Utils.getTemplateAnswersForBot1().length)];
    }

    @Override
    public String stop() {
        return super.stop();
    }
}
