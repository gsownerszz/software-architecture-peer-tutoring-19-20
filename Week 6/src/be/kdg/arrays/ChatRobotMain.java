package be.kdg.arrays;

import be.kdg.arrays.chat.robot.*;

import java.util.Scanner;

public class ChatRobotMain {
    private static Scanner keyboard = new Scanner(System.in);
    private static ChatRobot robot;
    private static int profanityCount = 0;
    public static void main(String[] args) {
        chooseRobotType();
        showWelcomeMessage();
        askForQuestions();
        showStopMessage();
        showInvoice();
    }

    private static void chooseRobotType(){
        boolean ok = false;
        while (!ok){
            try {
                System.out.println("Welkom in de support systeem. Welke type robot wil je spreken:");
                int type = keyboard.nextInt();
                switch (type){
                    case 1: robot = new ChatRobotVersion1("Dieter");
                        ok = true;
                        break;
                    case 2: robot = new ChatRobotVersion2("Jan");
                        ok = true;
                        break;
                    case 3: robot = new ChatRobotVersion3("Bart");
                        ok = true;
                        break;
                    default:
                        ok = false;
                }
            }catch (Exception e){
                ok = false;
                keyboard = new Scanner(System.in);
            }

        }
    }
    private static void showWelcomeMessage(){
        System.out.println(robot.toString());
        System.out.println("Tik \"stop\" om te eindigen");
    }
    private static void showStopMessage(){
        System.out.println(robot.stop());
    }
    private static void askForQuestions(){
        boolean stop = false;
        while (!stop){
            String vraag = keyboard.nextLine();
            if (vraag.equalsIgnoreCase("stop")){
                stop = true;
            }else if(Utils.detectProfanity(vraag)){
                System.out.println("Gelieve beleefd te blijven!!!");
                profanityCount++;
            }
            else {
                System.out.println(robot.antwoordOpVraag(vraag));
            }
        }
    }
    private static void showInvoice(){
        int extraFifteenSeconds = Math.round(robot.getExtraSeconds())/15;
        int profanityCost = profanityCount * 5;
        int startCost = 10;
        int total = startCost + profanityCost + extraFifteenSeconds;
        System.out.println("Factuur:");
        System.out.println(String.format("%-15s %5s","Item","Prijs"));
        System.out.println("=".repeat(21));
        System.out.println(String.format("%-15s %5s","Opstart","€ " + startCost));
        System.out.println("Extra time:");
        System.out.println(String.format("%-15s %5s",String.format("%dx15seconden",extraFifteenSeconds),"€ " + extraFifteenSeconds));
        System.out.println(String.format("%-15s %5s","Scheldboete:", "€ " + profanityCost));
        System.out.println(String.format("%-15s %5s","Totaal:",total));
        System.out.println();
        System.out.println("Gelieve binnen de 2 werkdagen te betalen.");
    }
}
