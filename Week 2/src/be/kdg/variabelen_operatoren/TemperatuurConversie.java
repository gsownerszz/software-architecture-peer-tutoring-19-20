package be.kdg.variabelen_operatoren;

import java.util.Scanner;

public class TemperatuurConversie {
    private static boolean exit;
    private static Scanner keyboard = new Scanner(System.in);
    public static void main(String[] args) {
        while (!exit){
            optionPicker(showOptions());
        }
        System.out.println("Tot ziens!");
    }
    private static int showOptions(){
        System.out.println("Conversie graden Celsius - Fahrenheit");
        System.out.println("=====================================");
        System.out.println("Welke conversie wens je te doen?");
        System.out.println("\t 0) Sluit applicatie");
        System.out.println("\t 1) °C naar °F");
        System.out.println("\t 2) °F naar °C");
        System.out.println("\t 3) °C naar °F met stapwaarde");
        System.out.println("\t 4) °C naar °F met stapwaarde in tabelvorm");
        System.out.print("Uw keuze?");
        return keyboard.nextInt();
    }
    private static void optionPicker(int option){
        switch (option){
            case 0:
                exit = true;
                break;
            case 1:
                System.out.println(showCelsiusToFahrenheit(requestCelcius()));
                break;
            case 2:
                System.out.println(showFahrenheitToCelsius(requestFahrenheit()));
                break;
            case 3:
                showCelsiusToFahrenheitWithSteps();
                break;
            case 4:
                showCelsiusToFahrenheitWithStepsInTable();
                break;
        }
    }

    private static int requestCelcius(){
        System.out.print("Geef de waarde in °C:");
        return keyboard.nextInt();
    }
    private static int requestFahrenheit(){
        System.out.print("Geef de waarde in °F:");
        return keyboard.nextInt();
    }
    private static int requestStepValue(){
        System.out.print("Geef de stapwaarde:");
        return keyboard.nextInt();
    }
    private static float convertCelsiusToFahrenheit(int celsius){
        return (float) (celsius * 9/5) + 32;
    }
    private static float convertFahrenheitToCelsius(int fahrenheit){
        return (float)  (fahrenheit - 32) * 5/9;
    }
    private static String showCelsiusToFahrenheit(int celsius){
        return String.format("%d°C = %.2f°F", celsius, convertCelsiusToFahrenheit(celsius));
    }
    private static String showFahrenheitToCelsius(int fahrenheit){
        return String.format("%d°F = %.2f°C", fahrenheit, convertFahrenheitToCelsius(fahrenheit));
    }
    private static void showCelsiusToFahrenheitWithSteps(){
        int start = requestCelcius();
        int end = requestCelcius();
        int step = requestStepValue();
        if (start > end){
            System.out.println("De begintemperatuur moet kleiner zijn dan de eindtemperatuur!");
        }else {
            for (int i = start; i <= end ; i+= step) {
                System.out.println(showCelsiusToFahrenheit(i));
            }
        }

    }
    private static void showCelsiusToFahrenheitWithStepsInTable(){
        int start = requestCelcius();
        int end = requestCelcius();
        int step = requestStepValue();
        if (start > end){
            System.out.println("De begintemperatuur moet kleiner zijn dan de eindtemperatuur!");
        }else {
            System.out.println("===============");
            System.out.println("|  °C  |  °F  |");
            System.out.println("---------------");
            for (int i = start; i <= end ; i+= step) {
                System.out.println(String.format("|%5d |%6.2f|",i,convertCelsiusToFahrenheit(i)));
            }
            System.out.println("===============");
        }

    }



}
