package be.kdg.klassen.golven;

import java.awt.*;
import java.util.Random;

public class GolvenGrafiek {
    private int aantal;
    private Random random;
    public GolvenGrafiek(int aantal){
        this.aantal = aantal;
        random = new Random();
    }
    public void tekenGolven(){
        GrafiekWindow window = new GrafiekWindow(10,6);
        for (int i = 1; i <= aantal; i++) {
            Golf golf = new Golf();
            golf.setAmplitude(getDoubleBetween(0.0,4.0));
            golf.setFrequentie(getDoubleBetween(0.0,4.0));
            Color golfColor = getRandomColor();
            for (double j = -5; j <= 5; j += 0.001) {
                window.tekenPunt(j,golf.getYwaarde(j), golfColor);
            }
        }
        window.toon();
    }
    private double getDoubleBetween(double min, double max){
        return  min + (max - min) * random.nextDouble();
    }
    private Color getRandomColor(){
        float r = random.nextFloat();
        float g = random.nextFloat();
        float b = random.nextFloat();
        return new Color(r,g,b);
    }
}
