package be.kdg.klassen.golven;

public class Golf {
    private double amplitude;
    private double frequentie;

    public Golf() {
        this.amplitude = 1.0;
        this.frequentie = 1.0;
    }

    public double getAmplitude() {
        return amplitude;
    }

    public void setAmplitude(double amplitude) {
        if (Double.compare(this.amplitude,amplitude) != 0){
            this.amplitude = amplitude;
        }
    }

    public double getFrequentie() {
        return frequentie;
    }

    public void setFrequentie(double frequentie) {
        if (Double.compare(this.frequentie,frequentie) != 0){
            this.frequentie = frequentie;
        }
    }
    public double getYwaarde(double x){
        return amplitude * Math.sin(frequentie * x);
    }

    @Override
    public String toString() {
        String amplitude = "";
        String frequentie = "";
        if (this.amplitude != 1){
            amplitude = String.format("%.1f",this.amplitude);
        }
        if (this.frequentie != 1){
            frequentie = String.format("%.1f",this.frequentie);
        }
        return "y = " + amplitude + " sin (" + frequentie + " x)";
    }
}
