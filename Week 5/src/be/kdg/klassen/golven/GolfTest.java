package be.kdg.klassen.golven;

public class GolfTest {
    private static double amplitude = 1;
    private static double frequentie = 2.0;
    public static void main(String[] args) {
        for (int i = 0; i < 5 ; i++) {
            Golf golf = new Golf();
            golf.setAmplitude(amplitude);
            golf.setFrequentie(frequentie);
            System.out.println(golf);
            amplitude +=0.5;
        }
        GolvenGrafiek grafiek = new GolvenGrafiek(10);
        grafiek.tekenGolven();
    }
}
