package be.kdg.expressies_statements_contructies;

import java.util.InputMismatchException;
import java.util.Scanner;

public class BloedDonatie {
    private static final Scanner keyboard = new Scanner(System.in);
    private static final double MIN_DONATION = 0.45;
    public static void main(String[] args) {
        routineQuestions();
    }
    private static void checkForTatoo(){
        System.out.print("Heb je de laatste 4 maanden een tatoeage laten zetten? (J/N):");
        char tatoo = keyboard.nextLine().toLowerCase().charAt(0);
        keyboard.nextLine();
        if (tatoo == 'j'){
            notAllowedToGiveBlood();
        }
    }
    private static String getGender(){
            System.out.print("Man of Vrouw? (M/V):");
            char geslacht = keyboard.nextLine().toLowerCase().charAt(0);
           keyboard.nextLine();
            if (geslacht == 'm'){
                maleRoute();
                return "m";
            }else if (geslacht == 'v'){
                femaleRoute();
                return "v";
            }else {
              return getGender();
            }
    }
    private static void notAllowedToGiveBlood(){
        System.out.println("Jammer, je komt niet in aanmerking om bloed te geven.");
        System.exit(0);
    }
    private static void routineQuestions(){
        checkForTatoo();
        String gender = getGender();
        System.out.println("Wat is uw leeftijd?");
        int age = keyboard.nextInt();
        if (age < 18){
            notAllowedToGiveBlood();
        }

        System.out.println("Laatse bloedgifte");
        int jarenGeleden = keyboard.nextInt();
        if (jarenGeleden ==0 && age >= 66){
            notAllowedToGiveBlood();
        } else if (jarenGeleden <= 3 && age >= 71){
            notAllowedToGiveBlood();
        }
        if (gender.equalsIgnoreCase("m")){
            showIfDonationIsAllowed(calculateMaleBloodVolume( askLength(),askWeight()));
        }else {
            showIfDonationIsAllowed(calculateFemaleBloodVolume( askLength(),askWeight()));
        }
    }
    private static void maleRoute(){
        System.out.println("Had je seksuele betrekkingen met een andere man? (J/N)");
        char antwoord = keyboard.nextLine().toLowerCase().charAt(0);
        keyboard.nextLine();
        if (antwoord == 'j'){
            notAllowedToGiveBlood();
        }
    }
    private static void femaleRoute(){
        System.out.println("Bent u zwanger? (J/N)");
        char antwoord = keyboard.nextLine().toLowerCase().charAt(0);
        keyboard.nextLine();
        if (antwoord == 'j'){
            notAllowedToGiveBlood();
        }
    }
    private static double askLength(){

            System.out.println("Wat is je lengte (in m):");
            return keyboard.nextDouble();


    }
    private static double askWeight(){
        System.out.println("Wat is je gewicht (in kg):");
        return keyboard.nextDouble();
    }

    private static double calculateMaleBloodVolume(double length, double weight){
        return (0.3669* Math.pow(length,3)) + (0.03219*weight + 0.6041);
    }
    private static double calculateFemaleBloodVolume(double length, double weight){
        return (0.3561* Math.pow(length,3)) + (0.03308*weight + 0.1833);
    }
    private static void showIfDonationIsAllowed(double bloodVolume){
        double maxDonation = 0.13 * bloodVolume;
        System.out.println("Bloed volume: " + bloodVolume);
        System.out.println("Max donatie: " + maxDonation);
        System.out.println("Min donatie: " + MIN_DONATION);
        if (maxDonation < MIN_DONATION){
            notAllowedToGiveBlood();
        }
        System.out.println("Je mag wel bloed geven");
        showIfDonationIsAllowedGraph(bloodVolume);
    }
    private static void showIfDonationIsAllowedGraph(double bloodVolume){
        double maxDonation = 0.13 * bloodVolume;
        String output = "";
        for (double i = 0; i <= bloodVolume; i+= 0.1) {
            output +="*";
        }
        System.out.printf("%15s %.2f %-50s \n", "Bloed volume:", bloodVolume, output);
        output="";
        for (double i = 0; i <= maxDonation; i+= 0.1) {
            output +="*";
        }
        System.out.printf("%15s %.2f %-50s \n", "Max donatie:", maxDonation, output);
        output="";
        for (double i = 0; i <= MIN_DONATION; i+= 0.1) {
            output +="*";
        }
        System.out.printf("%15s %.2f %-50s \n", "Min Donatie:", MIN_DONATION, output);
    }

}
