import rekenen.*;

import java.util.Scanner;
import rekenen.plugins.*;

/**
 * PEER TUTORING
 * P2W3
 */
public class TestRekenmachine {
    private static Rekenmachine mijnCalc = new Rekenmachine();
    private static Scanner input = new Scanner(System.in);
    public static void main(String[] args) {
        mijnCalc.installeer(new Optelling());
        mijnCalc.installeer(new Aftrekking());
        mijnCalc.installeer(new Vermenigvuldiging());
        mijnCalc.installeer(new Deling());
        mijnCalc.installeer(new Macht());
        mijnCalc.installeer(new Plugin() {
            @Override
            public String getCommand() {
                return "MIN";
            }

            @Override
            public double bereken(double x, double y) {
                return Math.min(x, y);
            }
        });
        mijnCalc.installeer(new Plugin() {
            @Override
            public String getCommand() {
                return "MAX";
            }

            @Override
            public double bereken(double x, double y) {
                return Math.max(x, y);
            }
        });

        //Opgave 3.3

        doeBerekeningEnDrukAf("+", 5, 2);
        doeBerekeningEnDrukAf("-", 5, 2);
        doeBerekeningEnDrukAf("*", 5, 2);
        doeBerekeningEnDrukAf("/", 5, 2);
        doeBerekeningEnDrukAf("^", 5, 2);
        doeBerekeningEnDrukAf("?", 5, 2);
        System.out.println(mijnCalc.toString());


        //Opgave 3.2
        createWhiteSpace();
        System.out.println("Welkom bij de dynamische rekenmachine!");
        System.out.println(mijnCalc.toString());
        askUserInput();
        System.out.println(mijnCalc.getLog());;
    }
    private static void askUserInput(){
        boolean quit = false;
        while (!quit){
          String command =  requestCommand();
            if (command.equals("")){
                quit = true;
            }else {
               double[] numbers =  requestNumbers();
               double x = numbers[0];
               double y = numbers[1];
               doeBerekeningEnDrukAf(command,x,y);
            }
        }
    }
    private static String requestCommand(){
        System.out.print("Welke berekening wenst U uit te voeren (<ENTER> om te stoppen)? ");
        return input.nextLine();
    }
    private static void createWhiteSpace(){
        for (int i = 0; i < 20 ; i++) {
            System.out.println();
        }
    }
    private static double[] requestNumbers(){
        boolean ok = false;
        double[] out = new double[2];
        while (!ok){
            System.out.print("Geef twee getallen in (gescheiden door een spatie): ");
            String numbers = input.nextLine();
            if (numbers.split(" ").length == 2){
                try {
                    out[0] = Double.parseDouble(numbers.split(" ")[0]);
                    out[1] = Double.parseDouble(numbers.split(" ")[1]);
                    ok = true;
                }catch (Exception e){
                    ok  = false;
                }
            }
        }
        return out;
    }

    //Opgave 3.1
    private static void doeBerekeningEnDrukAf(String commando, double getal1, double getal2){
        System.out.printf("%.2f %s %.2f = %.2f\n"
                , getal1, commando, getal2
                , mijnCalc.bereken(commando, getal1, getal2));
    }




}
