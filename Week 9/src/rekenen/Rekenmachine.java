package rekenen;

import rekenen.plugins.Plugin;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * PEER TUTORING
 * P2W3
 */
public class Rekenmachine {
    private final int MAX_AANTAL_PLUGINS = 10;
    private Plugin[] ingeladenPlugins;
    private int aantalPlugins;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
    private StringBuilder log;

    public Rekenmachine() {
        this.ingeladenPlugins = new Plugin[MAX_AANTAL_PLUGINS];
        aantalPlugins = 0;
        this.log = new StringBuilder();
    }

    public void installeer(Plugin teInstallerenPlugin) {
        //Opgave 2.1.a
        if (!isAlreadyInstalled(teInstallerenPlugin.getCommand()) && aantalPlugins < MAX_AANTAL_PLUGINS){
           ingeladenPlugins[aantalPlugins] = teInstallerenPlugin;
           aantalPlugins++;
        }
    }

    public double bereken(String command, double x, double y) {
        //Opgave 2.1.b
        if (isAlreadyInstalled(command)){
            double result = getPlugin(command).bereken(x, y);
            logCalculation(command,x,y,result);
            return result;
        }
        System.out.println(String.format("Commando: %s niet geinstalleerd", command));
        return 0;
    }
    private Plugin getPlugin(String command){
        for (Plugin plugin:ingeladenPlugins) {
            if (plugin.getCommand().equalsIgnoreCase(command)){
                return plugin;
            }
        }
        return null;
    }
    private boolean isAlreadyInstalled(String command){
        if (aantalPlugins != 0) {
            for (Plugin element : ingeladenPlugins) {
                if (element != null && element.getCommand().equalsIgnoreCase(command)) {
                    return true;
                }
            }
        }
        return  false;
    }
    private void logCalculation(String command, double x, double y, double result){
        this.log.append(String.format("[%s] %f %s %f = %f \n",dateFormat.format(new Date()).toLowerCase(),x, command,y, result));
    }
    public String getLog(){
        return "==== LOG ====\n" + this.log.toString();
    }

    @Override
    public String toString() {
        //Opgave 2.1c
        StringBuilder output = new StringBuilder();
        if (aantalPlugins ==0){
            output.append("Er zijn geen plugins geinstalleerd");
            return output.toString();
        }
        output.append("Geïnstalleerde Plugins: ");
        for (Plugin plugin: ingeladenPlugins) {
            if (plugin != null){
                output.append(plugin.getCommand()+ " ");
            }
        }
        return output.toString();
    }
}
